FROM node:8.9.0-alpine

ENV NPM_CONFIG_LOG_LEVEL warn

RUN npm install -g yarn@1.3.2 && mkdir /opt/app

ARG NODE_ENV=production
ENV NODE_ENV $NODE_ENV

ARG PORT=3000
ENV PORT = $PORT

WORKDIR /opt/app

COPY ./yarn.lock /opt/app
COPY ./package.json /opt/app

RUN yarn install

COPY . /opt/app

CMD ['yarn', 'start']

